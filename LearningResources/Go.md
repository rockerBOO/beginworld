## Learnings/Go_Resources.md

- Go By Example
  - https://gobyexample.com/
- Effective Go
  - https://golang.org/doc/effective_go.html
- Go in Action
  - Co Written By THE Erik Saint Martin
- Dave Cheney Blog
- Most Underrated: Go Standard Library Code
- https://cloud.google.com/apis/design/
- Black Hat Go
