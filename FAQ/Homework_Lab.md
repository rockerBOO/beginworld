# Creating a Home/Work Lab

- Create a general sketch of some applications similar to ones at work,
  make them goofier
- List out some of the tech you are using and plan on continuing using
- List out some tech you want to try out

---

Home/Work Lab

Sandbox <- its not for others, or like isolated. It's a chaotic lab.

---

Stitch them together,  to create a Worklike Playground:

Things For the Playground:
  - New Deployment Techniques
  - New Logging
  - Any new Tool
  - Anything new language, library, framework

This is were you go to run experiments for new things, to bring
into a company.

Example:

- Rust! I want to work in Rust
  -> think of purpose for it.
    -> Logger in Rust
      We need to, its soooo fast, we have sooo many logs, enterprise scale logs
    -> build one in your lab.

Think through the annoying stuff:
  -> How do logs look from services, databases, other places
