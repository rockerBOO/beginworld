# FAQ/Networking_for_weirdos


## Idea:

- You network sooo little in person anyway

What are your communities?

## Examples of Communities

- Open Source Project on Github/lab
- Discords/slacks/gitters/IRCs/Subreddits
- Twitch Channel community
- Area specific community (Houston)
- Cross-Communities (music/programming)
  - Its easier to make friends, when you're new to programming if you
    have something else in common
