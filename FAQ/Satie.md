## Satie

Here is the exact timetable of my daily activities.

Get up: 7:18 am; be inspired 10:23 to 11:47 am.

Take lunch: 12:11 pm; leave table at 12:14 pm.

Healthy horse-riding out in the grounds: 1:19 to 2:53 pm

More inspiration: 3:12 to 4:07 pm.

Various activities (fencing, reflection, immobility, visits, contemplation, swimming etc.): 4:21 to 6:47 pm.

Dinner is served at 7:16 and ends at 7:20 pm

Then come symphonic readings out loud 8:09 to 9:49 pm.

I go to bed regularly at 10:37 pm. Once a week on Tuesdays I wake with a start at 3:19 am

I can only eat white foods: eggs, sugar, scraped bones, fat from dead animals, veal, salt, coconuts, chicken cooked in white water, rice, turnips, things like pasta, white cheese, cotton salad and certain fish.

I boil my wine and drink it cold mixed with fuchsia juice. I have a good appetite, but never talk while eating, for fear of strangling myself.

I breathe carefully a little at a time.

My sleep is deep but I keep one eye open. My bed is round with a hole cut out to let my head through. Once every hour a servant takes my temperature.

I have long subscribed to a fashion magazine. I wear a white bonnet, white stockings and a white waistcoat.

My doctor has always told me to smoke. Part of his advice runs “Smoke away, my dear chap. If you don’t someone else will.”

- Erik Satie

