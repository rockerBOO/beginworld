# Why Take Notes

- That one command that made everything work
- A list of all the things you tried when debugging
- Things to explore Later
- Look back over time, and notice themes, in the things that are slowing
  you down or confusing you.
