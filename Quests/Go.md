# Drink all the Gopher Koolaid

## Go Questions/Ponderings

- I am divded up pkgs/ into separate files
  since it feels easier to manage/organize things
  however, not sure what the Gopher way

Why do we like these one-liners?
```
if err := conn.WriteMessage(messageType, p); err != nil {
}
```

## Favorite thing about Go

The Phrase: "Go is biased against unnecessary abstractions"

...This makes working in complicated code bases, more obvious.
Might be more boiler plater, but less magic to get lost in

aquafunkalisticbootywhap: what's a necessary vs unnecessary abstraction (asking seriously)
beginbot: abstracting incorrectly
  ...these things look alike...but really aren't
abstracting too early

## Go Emotional Issues

- Functions with long signatures, hard to read

- Gofmt is supposed to do it all for me
.....which implies my code is bad maybe

....Do you ever do more formatting, am I following some dumb styles???

## Always Do This

All functions return an error as the right most return argument

## Things I Like

- Simplicity of the language
- Formatting is built-into language
- Concurrency primitives are super dank
  - Easy to launch lots of "threads" and have them communicate with each other
    in safe manner
- "Go is biased against unnecessary abstractions"
  - Becasue of this, Go code bases are always obvious, you aren't
    constantly look up different DSLs and abstractions

## Things I don't (which I might like)

- Boiler Plate!!! You are writing the same thing (error handling etc, over and
  over again)
- The module system update, make searching about Go dependency managment
  annoying


## Gopher Problems

- DO NOT NAME PACKAGES THE SAME AS BUILTIN PACKAGES

## Questions

- Can you check the length of whats in a channel, without consuming it??
- What are the best practices for testing channels are written to,
  without the potential to block you tests?

## The Struggle become a Gopher

- How do I develop the methodology to determining unbuffered VS buffered,
  and the what buffer size

## Gopher Debates

- Where do you define your structs and interfaces

Option 1:
  - at the top of the file
Option 2:
  - Next to where they are used

## Cool Names

- CTRL (pronouced Carl)
- MQTT (pronouced Matt)
  -> Mott
  -> Mutt
  -> Matt

## Things I want to learn

- testing by building "test" interfaces
  - Suggest reading the source for the tests for the actual net library.
  - https://stackoverflow.com/questions/30688685/how-does-one-test-net-conn-in-unit-tests-in-golang

It feels a little weird to me, to create a "mock" irc server, just to test

We should
reader io.Reader

MOCKING AN IO.READER IN UNIT TESTS FOR GO

## Interesting message

call to (*T).Fatal from a non-test goroutine

## Gopher Recommendations

garionion: @erikdotdev what golang gstreamer binding lib would you recommend?

## Tooling TODO

- Start using Delve

## Global Begin Writing Go Rules

- NEVER SHADOW A VARIABLE
- ALWAYS PRINT OR HANDLE THE ERROR

## Why Learn Go if I know Python

- Go is Excellent at concurrency
- And you can spend more time learning about different patterns
  and concepts in concurrency, versus learning the python
  language way of handling concurrency
- You have some slow python, you want to make faster

## What is Go Good For?

- Backend Services
- CLIs
- Concurrency Is A Dream
- Force a Simplistic Style that leads to non-magical, obvious, scalable code

## Examples

- Docker
- Kubernetes
- Terraform
- Vault
- Github CLI
- Many Companies Backends

## GO Garbage Collection

erikdotdev:
  Go has a runtime.
  It manages your memory.
  You can reduce allocations,
  but you don’t have the control you do in C/C++ about when and how your memory is deallocated.

## Go Mysteries

```go
count, err := db.Model((*models.Player)(nil)).Count()

// What the heck is this!?!?!?
(*models.Player)(nil))
```

!dropeffect 50 ctrlvandergeest

- HOW CAN I GET ALL ERRORS TO SHOW FOR A GO BUILD?

Go Test output, hard to read:
  - Need Colors
  - Need to see what easily failed

## Open Questions

- How do you debug long `go build .` times?


When types are so long, is it responsible to make a struct for this??
Or is that some old programming habit
```
func CommandsJsonToPG(db *gorm.DB, jsonCommands map[string]map[string]models.StreamCommand) []models.StreamCommand {
```


## Go Learnings

lucasfernsilva: Begin, as I told you yesterday I am learning GO, and I don't know the difference between Printf and Println, can you explain to me?

Println VERSUS Printf:
  - ln has a \n
  - printf, you interpolate varialbes with the string interpolation syntax %s,
    %v, %d
  - Println, you cannot concat strings fmt.Println("One Strang", " OTHER STRANG", aVariable, "MORE STRINGS")

## Go Zeitgeist

- People hate the builder pattern in Go
  - Doesn't work well with type system
  - constructing interfaces that interact with the builder pattern
    is hard
- Certain libraries are trying to bring another language experience
  over, but it boxes it from interfacing with the rest of the system.
  - When I use go-pg, I pass a go-pg specific type around,
    instead of database/sql interface.

## Begin Theories

...I don't know too many people
...but now I know Prime and Teej
...and I noticed some patterns

We all 3 Use vim and Stream
Teej and Prime have kids
...whats different

Teej and Prime: Chad Ubuntu Users
Cool Beginbot: Virgin Incel Arch User

....when I switch to the Chad Ubuntu, how long until Im married with Kids???
