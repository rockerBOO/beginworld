# Quests/I3.md


westar: Is there a way to get a window across workspaces? Like both on 1 and 2?
Same size and everything
beginbot: With i3? ...No?
beetle3d: no more clone
westar: Not streching
carlvandergeest: he's talking about cloning
westar: I want to have a window persist while switching between workspaces.
If I could figure that out, i3 would become viable
beginbot: make a GIF or video showing what you mean

## Can I3 do this?

https://youtu.be/mXZER8O-Kc4

punchypenguin: Hold the windows key + p, then select Duplicate
mccannch: Sticky floating windows If you want a window to stick to the glass, i.e., have it stay on screen even if you switch to another workspace, you can use the sticky command. For example, this can be useful for notepads, a media player or a video chat window.
