# Go Local Importing

```go.mod
module gitlab.com/beginbot/beginsounds

go 1.15

require (
	github.com/agnivade/levenshtein v1.1.0
	github.com/go-pg/pg v8.0.7+incompatible
	github.com/go-pg/pg/v10 v10.3.2
	github.com/golang/mock v1.4.4
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.8.0
	github.com/veandco/go-sdl2 v0.4.4
)
```
The top line `module: module gitlab.com/beginbot/beginsounds`

This line, is what allows us to import local files,
and separate things into modules

gitlab.com/beginbot doesn't matter
I choose to add that
if its not on gitlab, it will still work
...I could lie and say what its on
gitlab/

```main.go
package main

import gitlab.com/beginbot/beginsounds/pkg/chat

func main() {
  chat.ChatStuff()
}
```

```main.go
package chat

func ChatStuff() {
  chat.ChatStuff()
}
```
